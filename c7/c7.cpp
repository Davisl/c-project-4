// 9/28/2019
//CIT 245 Data Structures and Algorithms C++
//Leland Davis

#include <iostream>

using namespace std;

class counter
{
public:
	counter();
	counter(int x);

	static int num;

	int getX();
	int dec();
	int inc();
	static int getnum();
	
	
private:
	int x = 0;
};
counter::counter()
{}
counter::counter(int xval) : x(xval)
{}
int counter::getX()
{
	return x;
}

int counter::num = 0;

int main()
{
	int count1 = 85;
	int count2 = 242;

	counter c1, c2(200);

	cout << "Current state of c1\n";
	cout << "Current count: " << c1.getX() << endl;
	cout << "Total increments and decrements " << c1.num << endl;
	cout << "Current state of c2\n";
	cout << "Current count: " << c2.getX() << endl;
	cout << "Total increments and decrements " << c2.num << endl;

	
	for (int a = count1; a > 0; a--) {
		counter::getnum();
		c1.inc();
	}

	cout << "Current state of c1 after 85 inc()" << endl;
	cout << "Current count: " << c1.getX() << endl;
	cout << "Total increments and decrements " << c1.num << endl;


	for (int b = count2; b > 0; b--) {
		counter::getnum();
		c2.dec();
	}

	cout << "Current state of c2 after 242 dec()" << endl;
	cout << "Current count: " << c2.getX() << endl;
	cout << "Total increments and decrements " << c2.num << endl;

	system("pause");
}

int counter::getnum()
{
	num++;
	return num;
}

int counter::inc()
{
	x++;
	return x;
}

int counter::dec()
{
	x--;
	return x;
}